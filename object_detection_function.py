# -*- coding: utf-8 -*-
"""


@author: CAMILO CASTRO 2420171012 , JENNYFER SALCEDO 2420151022
"""

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
from distutils.version import StrictVersion
from IPython import get_ipython

# ## Env setup


# desplegar imagenes
get_ipython().run_line_magic('matplotlib', 'inline')

sys.path.append("..")


# ## importar label map 
#


from utils import label_map_util

from utils import visualization_utils as vis_util


# # preparacion del modelo

MODEL_NAME = 'inference_graph'

PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'
# ruta a  to frozen detection graph. modelo usado para la inferencia


# clases
PATH_TO_LABELS = 'training/labelmap.pbtxt'



# carga del modelo para las inferencias (resumen del modelo despues del training)



detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.compat.v1.GraphDef()
  with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


#cargar el labelmap

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)



#cargar la imagen en un arreglo

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


# # Elegir la imagen a testear


PATH_TO_TEST_IMAGES_DIR = 'test_images'
TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, 10) ]

# Re dimensionar para la salida.
IMAGE_SIZE = (12, 8)




with detection_graph.as_default():
  with tf.compat.v1.Session(graph=detection_graph) as sess:
    for image_path in TEST_IMAGE_PATHS:
      image = Image.open(image_path)
      
      # resultado de la imagen con la clase.
      image_np = load_image_into_numpy_array(image)
      # dimensiones 
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # cada box representa una parte de la imagen
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      
      # Puntuacion de la presicion
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Deteccion
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
      # Visualizacion de la imagen final
      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=8)
      plt.figure(figsize=IMAGE_SIZE)
      plt.imshow(image_np)

  



